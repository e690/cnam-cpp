# Rapport Sauvegarde

-------------------
Groupe:
- Yoann BERTUOL
- Rayane CHEKATT
- Edouard ZAMB
- Dalila MANSOURI

### Fonctionnement initial

Une sauvegarde est enregistrée toutes les secondes en parallèle d'une partie de Morpion, de Puissance 4 ou d'Othello. Une seule sauvegarde est possible dans notre application, et le démarrage d'une nouvelle partie supprimera la dite sauvegarde pour enregistrer la partie démarrée. Lorsqu'une partie est terminée, le fichier de sauvegarde est supprimé.
Lors du démarrage de l'application, si un fichier de sauvegarde est présent, nous pouvons choisir s'il on veut démarrer un nouveau jeu, ou charger la sauvegarde et reprendre notre partie.

### Données de la Sauvegarde

La sauvegarde est composée :

- du joueur qui jouera le prochain tour (`_currentPlayer: int`),
- de la grille complète (`_grid: int[]`),
- du pseudo des joueurs (`_playerOne: string` et `_playerTwo: string`),
- du type de jeu (`gameType: int`)

Nos énumérations étant de type `integer` nous avons préférés les enregistrer directement sous cette forme, pour les "caster" lors du chargement.

Le fichier de sauvegarde se nomme `save.json`, n'ayant qu'une sauvegarde pour toute l'application.

### Librairies utilisées

Les principales librairies utilisées pour gérer notre sauvegarde ont étés les librairies offertes par le framework Qt, proposant les types :

- `QFile` pour la gestion de fichier,
- `QString` pour utiliser le type `std::string` dans ces classes,
- `QIODevice` pour ouvrir et écrire dans notre fichier,
- `QByteArray` pour la lecture de notre fichier,
- `QJsonDocument` pour la gestion du fichier JSON

La sauvegarde utilisée est donc sous format JSON, et non en format Binaire, l'optimisation n'étant pas nécéssaire dans notre application, associé à l'utilisation de **Threads** pour la sauvegarde en temps réel. De plus ce format nous permets un phase de développement plus simple, permettant de préparer des fichiers de sauvegarde de tests.

### Explications Approfondies

Chaque objet ayant des informations lié à la sauvegarde sont devenus sérialisables grâce à l'implémentation de l'interface **ISerializable** : 

```cpp
class ISerializable
{
public:
    virtual void Read(const QJsonObject &json) = 0;
    virtual void Write(QJsonObject &json) const = 0;
};
```
Il a fallu tous leur donner un constructeur vide initialisant leurs membres par défaut pour que, lors du chargement, nous puissions les enrichir des données récupérées.

De plus notre point d'entrée, la classe `CellGameTwoPlayers` comprends deux autres méthodes `SaveGame` et `LoadGame` qui s'occupe d'initialiser l'ouverture du fichier et la création de l'objet `QJsonDocument` qui permettra soit de lire les données soit de les enregistrer.

```cpp
const std::string SAVE_FILE_NAME = "save.json";

void CellGameTwoPlayers::LoadGame() {
    QFile loadFile(QString::fromStdString(SAVE_FILE_NAME));

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
    }
    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    Read(loadDoc.object());
}

void CellGameTwoPlayers::SaveGame() {
    QFile saveFile(QString::fromStdString(SAVE_FILE_NAME));

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
    }

    QJsonObject cellGameObject;
    Write(cellGameObject);
    saveFile.write(QJsonDocument(cellGameObject).toJson());
}
```

#### Sauvegarde

La sauvegarde se fera alors de deux côtés : 

- la première sauvegarde se concentrera sur la sérialization des joueurs, et plus particulièrement de leurs noms. On sérialize alors la classe `TwoPlayersGame` directement, un `Player` ne connaissant s'il est premier ou deuxième joueur.
- la seconde, plus complexe, va serialiser la classe `CellGame` connaissant sa grille et le joueur actuelle. Il a fallut créer la méthode abstraite `className` pour transmettre l'énumération du jeu réel en tant que jeu jouée. La grille est sérialisée comme un tableau 1D et non 2D, car demandant plus de traitement et devra être changé dans la suite du projet.

Après ce traitement, notre `QJsonDocument` passé par référence et composé de toutes nos données servira de source pour écrire dans notre fichier de sauvegarde.

Exemple avec la classe `TwoPlayersGame` :
```cpp
void TwoPlayersGame::Write(QJsonObject &json) const {
    json["_playerOne"] = QString::fromStdString(_playerOne.GetPseudo());
    json["_playerTwo"] = QString::fromStdString(_playerTwo.GetPseudo());
}
```

#### Chargement

Le fonctionnement inverse, le chargement, se fait sous la même forme :

- la classe `TwoPlayersGame` va récuperer l'objet `QJsonDocument` et va modifier les noms vides des deux joueurs.
- de la même manière, la classe `CellGame` retransforme le tableau 1D en 2D pour la grille (d'où la nécessité d'améliorer notre sauvegarde, il y a du traitement superflu) et rends son tour au joueur.

Il est à noter que, puisque l'on utilise un pointeur intelligent pour la création de notre jeu, il reste nul tant que l'on ne retrouve pas le type de jeu dans la sauvegarde. Il est alors instancié, puis modifié pour éviter des accès concurrents.

Exemple avec la classe `TwoPlayersGame` :
```cpp
void TwoPlayersGame::Read(const QJsonObject &json) {
    if(json.contains("_playerOne") && json["_playerOne"].isString()) _playerOne.SetPseudo(json["_playerOne"].toString().toStdString());
    if(json.contains("_playerTwo") && json["_playerTwo"].isString()) _playerTwo.SetPseudo(json["_playerTwo"].toString().toStdString());
}
```