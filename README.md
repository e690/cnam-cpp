# Solid

Yoann BERTUOL - Rayane CHECKATT / 09/11/2021
------------

### Choix du Projet

Après étude sommaire des deux projets, nous avons choisi de garder et d’étudier plus profondément le projet de **Yoann Bertuol**.Les deux projets ayant les deux jeux fonctionnels, la présence d’un héritage dans ce dernier a créé un écart dans le couplage du projet, ce qui nous à dirigé dans ce choix.
Nous allons alors ici développer les différents points de la méthode SOLID, appliqués au projet, ainsi que la Loi de Déméter et le principe “Tell don’t ask” pour justifier plus profondément notre choix.

### Single Responsibility

Ce principe se concentre sur une responsabilité unique de chaque section de notre code. Une méthode se concentrera sur une fonctionnalité et une classe n’aura qu’un objectif. Nous pouvons vérifier ceci dans la méthode ***IsWinning*** de la classe **CellGame**, qui s’occupe de vérifier la condition de victoire d’un jeu. Elle n’a qu’une seule utilité et sera utilisée dans les méthodes de vérification de la grille de jeu.
Ici la méthode la plus complexe est certainement la méthode ***IsDiagonalFull*** de la classe **PowerFour**. Malgré sa complexité et sa longueur, elle garde cette responsabilité unique de vérifier si une diagonale permet la victoire du joueur.

### Open / Closed

Le principe Open Closed est représenté ici par **CellGame** qui est la classe mère et qui est étendu par deux classes **Morpion** et **PowerFour** qui vont redéfinir les propriétés de la classe mère liée aux conditions de victoire du joueur. Cela rendra notre classe **CellGame** ouverte à l’extension, mais fermée à la modification de son code source qui lui est propre et sera propre aux classes filles.

### Liskov Substitution

Le principe de Liskov est ici vérifié par rapport à nos deux jeux **Morpion** et **PowerFour**. Le principe de Liskov prévient les surcharges de méthodes utilisées dans l'implémentation de nouvelles fonctionnalités. Lors de notre développement, nous avons d’abord travaillé sur le Morpion pour ensuite se concentrer sur le jeu de Puissance 4. Il aurait alors été tentant de faire hériter notre Puissance 4 de Morpion, et de réutiliser ce qui nous servait. Hors ce ne fut pas le cas, puisque cela briserait le principe de Liskov, et alors il a été privilégié une classe mère de ces jeux **CellGame**  ayant le commun de ces deux jeux, et le reste devant être redéfini par les classes filles. Cela permet de découpler tous les jeux fils de **CellGame** et d’éviter une complexité de surcharge de méthodes inutile.

### Interface Segregation

Ce projet n’utilise pas d’interface, nous ne pouvons alors pas vérifier ce principe SOLID.

### Dependency Injection

Une injection de dépendance de fait à deux niveau dans ce projet : 

- Le premier se trouve au niveau de la classe **CellGame** qui est fille de **TwoPlayersGame**. Une injection de dépendance se fait donc sur les joueurs participants au jeu.
- Des classes filles **Morpion** et **PowerFour** de **CellGame**, qui elles récupèrent les dépendances liées à la grille et ses dimensions.

On décentralise totalement les dépendances de ces jeux, permettant une implémentation simple par la suite pour le reste des jeux. Ils n’ont alors ici qu'à préciser les dimensions de la grille, et leurs conditions de victoire, et les dépendances s’occuperont donc du reste du fonctionnement du jeu.

### Loi de Demeter

La loi de Demeter semble être vérifiée dans l’implémentation des fonctionnalités du projet, pourtant dans son utilisation dans le fichier **Main.cpp** nous pouvons voir des appels de ce genre :

```
powerFour.GetPlayerOne().GetPseudo()
```

Il aurait été préférable d’avoir un appel de fonction `powerFour.GetPseudoPlayerOne()` pour éviter ces appels enchainées de méthode.
Nous pouvons donc considérer que l’implémentation ne valide pas cette loi car elle ne permet pas de la valider lors de son utilisation.

### "Tell don't ask"

Le principe “Tell don’t ask” ici est validé par le choix d’une Cellule par un Joueur par exemple. Ici le Joueur n’a qu'à sélectionner une Cellule, et c’est la méthode ***ChooseCell*** qui va s’occuper de faire la vérification, et ce n’est pas les méthodes utilisant cette fonction qui doit s’en occuper.

![](SOLID/Tell_don_t_ask.png)

### UML Complet

![](SOLID/SOLID_Architecture.png)
