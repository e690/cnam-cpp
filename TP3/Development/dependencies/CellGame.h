#ifndef CELLGAME_H
#define CELLGAME_H

#include <vector>

#include "TwoPlayersGame.h"
#include "./enums/State.h"

class CellGame : public TwoPlayersGame
{
public:

    CellGame(std::string pseudoPlayerOne, std::string pseudoPlayerTwo, int lines, int cols);
    void ChooseCell(State playerIndex, int cellLine, int cellColumn);
    std::string DisplayGrid() const;
    bool PlayerWon(State playerIndex) const;

    virtual void Play();
protected:
    int _nbLines;
    int _nbCols;
    std::vector<std::vector<State>> _grid;

    virtual bool IsLineFull(State playerIndex, int lineIndex) const = 0;
    virtual bool IsColumnFull(State playerIndex, int columnIndex) const = 0;
    virtual bool IsDiagonalFull(State playerIndex) const = 0;
    virtual bool IsWinning(std::vector<State> vector, State stateToVerify) const = 0;
    static bool CheckState(int currentState, State stateToVerify);
};

#endif // CELLGAME_H
