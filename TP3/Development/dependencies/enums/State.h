#ifndef STATE_H
#define STATE_H

enum State {UNUSED = 0, PLAYER_ONE = 1, PLAYER_TWO = 2};

#endif // STATE_H
