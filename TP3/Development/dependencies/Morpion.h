#ifndef MORPION_H
#define MORPION_H

#include "CellGame.h"
#include "Player.h"


class Morpion final : public CellGame
{
public:
    Morpion(std::string pseudoPlayerOne, std::string pseudoPlayerTwo);
private:
    bool IsLineFull(State playerIndex, int lineIndex) const;
    bool IsColumnFull(State playerIndex, int columnIndex) const;
    bool IsDiagonalFull(State playerIndex) const;
    bool IsWinning(std::vector<State> vector, State stateToVerify) const;
};

#endif // MORPION_H
