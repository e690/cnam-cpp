#ifndef POWERFOUR_H
#define POWERFOUR_H

#include "CellGame.h"

class PowerFour final : public CellGame
{
public:
    PowerFour(std::string pseudoPlayerOne, std::string pseudoPlayerTwo);
private:
    bool IsLineFull(State playerIndex, int lineIndex) const;
    bool IsColumnFull(State playerIndex, int columnIndex) const;
    bool IsDiagonalFull(State playerIndex) const;
    bool IsWinning(std::vector<State> vector, State stateToVerify) const;
};

#endif // POWERFOUR_H
