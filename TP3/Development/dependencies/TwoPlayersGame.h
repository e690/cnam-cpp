#ifndef TWOPLAYERSGAME_H
#define TWOPLAYERSGAME_H

#include "Player.h"

class TwoPlayersGame
{
public:
    TwoPlayersGame(std::string pseudoPlayerOne, std::string pseudoPlayerTwo);
    Player GetPlayerOne() const;
    Player GetPlayerTwo() const;
    virtual void Play() = 0;
protected:
    Player _playerOne;
    Player _playerTwo;
};

#endif // TWOPLAYERSGAME_H
