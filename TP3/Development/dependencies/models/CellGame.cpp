#include <iostream>

#include "../CellGame.h"

using namespace std;

CellGame::CellGame(string pseudoPlayerOne, string pseudoPlayerTwo, int lines, int cols) : TwoPlayersGame(pseudoPlayerOne, pseudoPlayerTwo) {
    _nbLines = lines;
    _nbCols = cols;
    _grid.resize(_nbLines, vector<State>(_nbCols, UNUSED));
}

bool CellGame::CheckState(int currentState, State stateToVerify) {
    return  currentState == stateToVerify;
}

void CellGame::ChooseCell(State playerIndex, int cellLine, int cellColumn) {
    State& state = _grid[cellLine][cellColumn];
    if(CheckState(state, UNUSED)) {
        state = playerIndex;
    } else {
        throw string("Cell already choosen.");
    }
}

string CellGame::DisplayGrid() const {
    string result = "";
    for (int i = 0; i < _nbLines ; i++) {
        for (int j = 0; j < _nbCols ; j++) result += to_string(_grid[i][j]);
        result += "\n";
    }
    return result;
}

bool CellGame::PlayerWon(State playerIndex) const {
    for(int i = 0; i < _nbLines; i++) if(IsLineFull(playerIndex, i)) return true;
    for(int i = 0; i < _nbCols; i++) if(IsColumnFull(playerIndex, i)) return true;
    if(IsDiagonalFull(playerIndex)) return true;
    return false;
}

void CellGame::Play() {
    State playerPlaying = PLAYER_ONE;
    int line = -1, column = -1;
    bool retry;

    cout << DisplayGrid() << endl;
    while(!(PlayerWon(PLAYER_ONE) || PlayerWon(PLAYER_TWO))) {
        cout << (playerPlaying == PLAYER_ONE ? GetPlayerOne().GetPseudo() : GetPlayerTwo().GetPseudo()) << "'s turn." << endl;
        retry = false;
        while(line < 0 || line >= _nbLines) {
            cout << "Choose line to place your Cell" << endl;
            cin >> line;
        }
        while(column < 0 || column >= _nbCols) {
            cout << "Choose column to place your Cell" << endl;
            cin >> column;
        }
        try {
            ChooseCell(playerPlaying, line, column);
        }  catch (string error) {
            cout << error << endl;
            retry = true;
        }

        if(!retry) {
            if(playerPlaying == PLAYER_ONE) playerPlaying = PLAYER_TWO;
            else playerPlaying = PLAYER_ONE;
        }
        line = -1;
        column = -1;
        cout << DisplayGrid() << endl;
    }
    if(PlayerWon(PLAYER_ONE)) cout << GetPlayerOne().GetPseudo() << " Won!" << endl;
    else cout << GetPlayerTwo().GetPseudo() << " Won!" << endl;
}
