#include "../PowerFour.h"

using namespace std;

static int __NB_LINE = 4;
static int __NB_COL = 7;

bool PowerFour::IsWinning(vector<State> vector, State stateToVerify) const {
    int counter = 0;
    for (State state : vector) {
        PowerFour::CheckState(state, stateToVerify) ? counter++ : counter = 0;
        if(counter == 4) return true;
    }
    return false;
}

PowerFour::PowerFour(string pseudoPlayerOne, string pseudoPlayerTwo) : CellGame(pseudoPlayerOne, pseudoPlayerTwo, __NB_LINE, __NB_COL) {}

bool PowerFour::IsLineFull(State playerIndex, int lineIndex) const {
    return IsWinning(_grid[lineIndex], playerIndex);
}
bool PowerFour::IsColumnFull(State playerIndex, int columnIndex) const {
    vector<State> subGrid;
    for (vector<State> vect : _grid) {
        subGrid.push_back(vect[columnIndex]);
    }
    return IsWinning(subGrid, playerIndex);

}
bool PowerFour::IsDiagonalFull(State playerIndex) const {
    vector<vector<State>> coordinatesBelongsToPlayer;
    vector<State> subGrid;
    int lineValue = 0, colValue = 0;
    bool boundModified = false;

    for (int line = 0; line < __NB_LINE; line++) for (int col = 0; col < __NB_COL; col++) {
        if(CheckState(_grid[line][col], playerIndex)) {
            subGrid.clear();
            for(int i = -4 ; i < 4; i++) {
                boundModified = false;
                if((line + i >= 0 && line + i < __NB_LINE)) {
                    boundModified = true;
                    lineValue = line + i;
                }
                if(col + i >= 0 && col + i < __NB_COL) {
                    boundModified = true;
                    colValue = col + i;
                }
                if(boundModified) {
                    subGrid.push_back(_grid[lineValue][colValue]);
                }
            }
            if(IsWinning(subGrid, playerIndex)) return true;

            subGrid.clear();
            for(int i = -4 ; i < 4; i++) {
                boundModified = false;
                if((line + i >= 0 && line + i < __NB_LINE)) {
                    boundModified = true;
                    lineValue = line + i;
                }
                if(col - i >= 0 && col - i < __NB_COL) {
                    boundModified = true;
                    colValue = col - i;
                }
                if(boundModified) {
                    subGrid.push_back(_grid[lineValue][colValue]);
                }
            }
            if(IsWinning(subGrid, playerIndex)) return true;
        }
    }
    return false;
}
