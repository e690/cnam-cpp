#include "../TwoPlayersGame.h"

using namespace std;

TwoPlayersGame::TwoPlayersGame(string pseudoPlayerOne, string pseudoPlayerTwo) {
    this->_playerOne = Player{pseudoPlayerOne};
    this->_playerTwo = Player{pseudoPlayerTwo};
}

Player TwoPlayersGame::GetPlayerOne() const {
    return _playerOne;
}

Player TwoPlayersGame::GetPlayerTwo() const {
    return _playerTwo;
}
