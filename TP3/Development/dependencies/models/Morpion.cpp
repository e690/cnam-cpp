#include "../CellGame.h"
#include "../Morpion.h"
#include "../Player.h"

using namespace std;

static int __NB_LINE = 3;
static int __NB_COL = 3;

bool Morpion::IsWinning(vector<State> vector, State stateToVerify) const {
    for (int state : vector) {
        if (!Morpion::CheckState(state, stateToVerify)) return false;
    }
    return true;
}

Morpion::Morpion(string pseudoPlayerOne, string pseudoPlayerTwo) : CellGame(pseudoPlayerOne, pseudoPlayerTwo, __NB_LINE, __NB_COL) {}

bool Morpion::IsLineFull(State playerIndex, int lineIndex) const {
    return IsWinning(_grid[lineIndex], playerIndex);
}

bool Morpion::IsColumnFull(State playerIndex, int columnIndex) const {
    vector<State> subGrid;
    for (vector<State> vect : _grid) {
        subGrid.push_back(vect[columnIndex]);
    }
    return IsWinning(subGrid, playerIndex);
}

bool Morpion::IsDiagonalFull(State playerIndex) const {
    vector<State> subGrid;
    for(int i = 0; i < __NB_LINE; i++) {
        subGrid.push_back(_grid[i][i]);
    }
    if(IsWinning(subGrid, playerIndex)) return true;

    subGrid.clear();
    for(int i = 0; i < __NB_LINE; i++) {
        subGrid.push_back(_grid[i][(__NB_LINE - 1) - i]);
    }
    if(IsWinning(subGrid, playerIndex)) return true;

    return false;
}
