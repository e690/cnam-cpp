#include <iostream>

#include "dependencies/Morpion.h"
#include "dependencies/PowerFour.h"

using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    //Morpion morpion = Morpion{"Yoann", "Yasmine"};
    //cout << "Player One: " << morpion.GetPlayerOne().GetPseudo() << endl;
    //cout << "Player Two: " << morpion.GetPlayerTwo().GetPseudo() << endl;
    //morpion.Play();
    PowerFour powerFour = PowerFour{"Yoann", "Yasmine"};
    cout << "Player One: " << powerFour.GetPlayerOne().GetPseudo() << endl;
    cout << "Player Two: " << powerFour.GetPlayerTwo().GetPseudo() << endl;
    powerFour.Play();
    return 0;
}
