QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    app/pages/sources/MenuChooseGame.cpp \
    main.cpp \
    app/pages/sources/Mainwindow.cpp \
    app/dependencies/sources/Cellgame.cpp \
    app/dependencies/sources/CellGameLineCondition.cpp \
    app/dependencies/sources/CellGameTwoPlayers.cpp \
    app/dependencies/sources/Morpion.cpp \
    app/dependencies/sources/Othello.cpp \
    app/dependencies/sources/Player.cpp \
    app/dependencies/sources/PowerFour.cpp \
    app/dependencies/sources/TwoPlayersGame.cpp \

HEADERS += \
    app/dependencies/enums/EnumGame.h \
    app/dependencies/interfaces/ISerializable.h \
    app/pages/headers/Mainwindow.h \
    app/dependencies/headers/Cellgame.h \
    app/dependencies/headers/CellGameLineCondition.h \
    app/dependencies/headers/CellGameTwoPlayers.h \
    app/dependencies/headers/Morpion.h \
    app/dependencies/headers/Othello.h \
    app/dependencies/headers/Player.h \
    app/dependencies/headers/PowerFour.h \
    app/dependencies/headers/TwoPlayersGame.h \
    app/dependencies/enums/State.h \
    app/pages/headers/MenuChooseGame.h

FORMS += \
    app/pages/forms/Mainwindow.ui \
    app/pages/forms/MenuChooseGame.ui

TRANSLATIONS += \
    translations/Project_en_150.ts
CONFIG += lrelease
CONFIG += embed_translations
CONFIG += console

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    app/ressources/img/CPP_LOGO.png

RESOURCES += \
    ressources.qrc
