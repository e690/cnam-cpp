#ifndef ISERIALIZABLE_H
#define ISERIALIZABLE_H

#include <QJsonObject>
#include <QJsonArray>

class ISerializable
{
public:
    virtual void Read(const QJsonObject &json) = 0;
    virtual void Write(QJsonObject &json) const = 0;
};

#endif // ISERIALIZABLE_H
