#include <iostream>

#include "../headers/Morpion.h"

using namespace std;

static const int __NB_LINE = 3;
static const int __NB_COL = 3;

bool Morpion::IsWinning(vector<State> vector, State stateToVerify) const {
    for (int state : vector) {
        if (!Morpion::CheckState(state, stateToVerify)) return false;
    }
    return true;
}

Morpion::Morpion() : CellGameLineCondition(__NB_LINE, __NB_COL) {}

bool Morpion::IsLineFull(State playerIndex, int lineIndex) const {
    return IsWinning(_grid[lineIndex], playerIndex);
}

bool Morpion::IsColumnFull(State playerIndex, int columnIndex) const {
    vector<State> subGrid;
    for (vector<State> vect : _grid) {
        subGrid.push_back(vect[columnIndex]);
    }
    return IsWinning(subGrid, playerIndex);
}

bool Morpion::IsDiagonalFull(State playerIndex) const {
    vector<State> subGrid;
    for(int i = 0; i < _nbLines; i++) {
        subGrid.push_back(_grid[i][i]);
    }
    if(IsWinning(subGrid, playerIndex)) return true;

    subGrid.clear();
    for(int i = 0; i < _nbLines; i++) {
        subGrid.push_back(_grid[i][(_nbLines - 1) - i]);
    }
    if(IsWinning(subGrid, playerIndex)) return true;

    return false;
}

EnumGame Morpion::className() const {
    return MORPION;
}

void Morpion::Play(string playerOneName, string playerTwoName) {
    int line = 0, column = 0, counterAction = 0;
    bool retry;

    cout << DisplayGrid() << endl;
    while(counterAction != 9 && !(PlayerWon(PLAYER_ONE) || PlayerWon(PLAYER_TWO))) {
        cout << (_currentPlayer == PLAYER_ONE ? playerOneName : playerTwoName) << "'s turn." << endl;
        retry = false;
        while(line < 1 || line > _nbLines) {
            cout << "Choose line to place your Cell" << endl;
            cin >> line;
        }
        while(column < 1 || column > _nbCols) {
            cout << "Choose column to place your Cell" << endl;
            cin >> column;
        }
        try {
            ChooseCell(line - 1, column - 1);
        }  catch (string error) {
            cout << error << endl;
            retry = true;
        }

        if(!retry) {
            if(_currentPlayer == PLAYER_ONE) _currentPlayer = PLAYER_TWO;
            else _currentPlayer = PLAYER_ONE;
        }
        line = -1;
        column = -1;
        counterAction++;
        cout << DisplayGrid() << endl;
    }
    if(counterAction == _nbLines * _nbCols) cout << "Equality." << endl;
    else if(PlayerWon(PLAYER_ONE)) cout << playerOneName << " Won!" << endl;
    else cout << playerTwoName << " Won!" << endl;
}
