#include "../headers/Player.h"

Player::Player() : _pseudo() {}

Player::Player(std::string pseudo) : _pseudo(pseudo) {}

std::string Player::GetPseudo() const {
    return this->_pseudo;
}

void Player::SetPseudo(std::string pseudo) {
    this->_pseudo = pseudo;
}
