#include <iostream>

#include "../headers/PowerFour.h"

using namespace std;

static const int __NB_LINE = 4;
static const int __NB_COL = 7;

bool PowerFour::IsWinning(vector<State> vector, State stateToVerify) const {
    int counter = 0;
    for (State state : vector) {
        PowerFour::CheckState(state, stateToVerify) ? counter++ : counter = 0;
        if(counter == 4) return true;
    }
    return false;
}

PowerFour::PowerFour() : CellGameLineCondition(__NB_LINE, __NB_COL) {}

bool PowerFour::IsLineFull(State playerIndex, int lineIndex) const {
    return IsWinning(_grid[lineIndex], playerIndex);
}
bool PowerFour::IsColumnFull(State playerIndex, int columnIndex) const {
    vector<State> subGrid;
    for (vector<State> vect : _grid) {
        subGrid.push_back(vect[columnIndex]);
    }
    return IsWinning(subGrid, playerIndex);

}

EnumGame PowerFour::className() const {
    return POWERFOUR;
}

bool PowerFour::IsDiagonalFull(State playerIndex) const {
    vector<vector<State>> coordinatesBelongsToPlayer;
    vector<State> subGrid;
    int lineValue = 0, colValue = 0;
    bool boundModified = false;

    for (int line = 0; line < _nbLines; line++) for (int col = 0; col < _nbCols; col++) {
        if(CheckState(_grid[line][col], playerIndex)) {
            subGrid.clear();
            for(int i = -4 ; i < 4; i++) {
                boundModified = false;
                if((line + i >= 0 && line + i < _nbLines)) {
                    boundModified = true;
                    lineValue = line + i;
                }
                if(col + i >= 0 && col + i < _nbCols) {
                    boundModified = true;
                    colValue = col + i;
                }
                if(boundModified) {
                    subGrid.push_back(_grid[lineValue][colValue]);
                }
            }
            if(IsWinning(subGrid, playerIndex)) return true;

            subGrid.clear();
            for(int i = -4 ; i < 4; i++) {
                boundModified = false;
                if((line + i >= 0 && line + i < _nbLines)) {
                    boundModified = true;
                    lineValue = line + i;
                }
                if(col - i >= 0 && col - i < _nbCols) {
                    boundModified = true;
                    colValue = col - i;
                }
                if(boundModified) {
                    subGrid.push_back(_grid[lineValue][colValue]);
                }
            }
            if(IsWinning(subGrid, playerIndex)) return true;
        }
    }
    return false;
}

void PowerFour::Play(string playerOneName, string playerTwoName) {
    int column = 0, counterAction = 0;
    bool retry;

    cout << DisplayGrid() << endl;
    while(!(PlayerWon(PLAYER_ONE) || PlayerWon(PLAYER_TWO))) {
        cout << (_currentPlayer == PLAYER_ONE ? playerOneName : playerTwoName) << "'s turn." << endl;
        retry = false;
        while (column < 1 || column > _nbCols) {
            cout << "Choose column to place your Cell" << endl;
            cin >> column;
        }
        if (_grid[0][column - 1] == UNUSED) {
            int i = _nbLines - 1;
            while (i >= 0) {
                try {
                    ChooseCell(i, column - 1);
                    break;
                }  catch (string error) {
                    if (i == 0) retry = true;
                    i--;
                }
            }
            if(!retry) {
                if(_currentPlayer == PLAYER_ONE) _currentPlayer = PLAYER_TWO;
                else _currentPlayer = PLAYER_ONE;
            }
            cout << DisplayGrid() << endl;
        } else cout << "Please choose a not filled column" << endl;
        column = 0;
    }
    if(counterAction == _nbLines * _nbCols) cout << "Equality." << endl;
    if(PlayerWon(PLAYER_ONE)) cout << playerOneName << " Won!" << endl;
    else cout << playerTwoName << " Won!" << endl;
}
