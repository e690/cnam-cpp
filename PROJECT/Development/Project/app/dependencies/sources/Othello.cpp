#include <iostream>
#include <algorithm>

#include "../headers/Othello.h"

using namespace std;

static const int __NB_LINE = 8;
static const int __NB_COL = 8;

Othello::Othello() : CellGame(__NB_LINE, __NB_COL) {
    // Set 4 middle cells on creation
    _grid[3][3] = PLAYER_TWO;
    _grid[4][4] = PLAYER_TWO;

    _grid[3][4] = PLAYER_ONE;
    _grid[4][3] = PLAYER_ONE;
}

string Othello::DisplayGrid() const {
    string result = "";
    vector<array<int, 2>> playableCells = WhereCanPlay();
    for (int i = 0; i < _nbLines ; i++) {
        for (int j = 0; j < _nbCols ; j++) {
            if(_grid[i][j] == UNUSED && find(playableCells.begin(), playableCells.end(), array<int, 2>{i, j}) != playableCells.end()) result += to_string(PLAYABLE);
            else result += to_string(_grid[i][j]);
        }
        result += "\n";
    }
    return result;
}

void Othello::ChooseCell(int cellLine, int cellColumn) {
    vector<array<int, 2>> playableCells = WhereCanPlay();
    if(find(playableCells.begin(), playableCells.end(), array<int, 2>{cellLine, cellColumn}) != playableCells.end()) CellGame::ChooseCell(cellLine, cellColumn);
    else throw string("Cell cannot be chosen.");
    FlipCells(cellLine, cellColumn);
}

void Othello::FlipCells(int cellLine, int cellColumn) {
    vector<array<int, 2>> coordinatesToFlip;
    array<int, 2> vectorTravel;
    array<State, 2> stoppingCondition = {_currentPlayer, UNUSED};
    for (int i = cellLine - 1 <  0 ? 0 : cellLine - 1 ; i <= (cellLine + 1 >= _nbLines ? _nbLines - 1 : cellLine + 1); i++) {
        int k = cellLine - 1 <  0 ? 0 : -1, l = cellColumn - 1 < 0 ? 0 : -1;
        for (int j = cellColumn - 1 < 0 ? 0 : cellColumn - 1; j <= (cellColumn + 1 >= _nbCols ? _nbCols - 1 : cellColumn + 1); j++) {
           if(find(begin(stoppingCondition), end(stoppingCondition), _grid[i][j]) == end(stoppingCondition)) {
               coordinatesToFlip.clear();
               vectorTravel = {k, l};
               int multiplier = 1;
               while (cellLine + vectorTravel[0] * multiplier >= 0
                      && cellColumn + vectorTravel[1] * multiplier >= 0
                      && find(begin(stoppingCondition), end(stoppingCondition), _grid[cellLine + vectorTravel[0] * multiplier][cellColumn + vectorTravel[1] * multiplier]) == end(stoppingCondition)) {
                   coordinatesToFlip.push_back({cellLine + vectorTravel[0] * multiplier, cellColumn + vectorTravel[1] * multiplier});
                   multiplier++;
               }
               if(cellLine + vectorTravel[0] * multiplier >= 0
                       && cellColumn + vectorTravel[1] * multiplier >= 0
                       &&_grid[cellLine + vectorTravel[0] * multiplier][cellColumn + vectorTravel[1] * multiplier] ==_currentPlayer) {
                   for(array<int, 2> coordinatesEntry : coordinatesToFlip) {
                       CellGame::ChooseCell(coordinatesEntry[0], coordinatesEntry[1]);
                   }
               }
           }
           l++;
        }
        k++;
    }
}

vector<array<int, 2>> Othello::WhereCanPlay() const {
    vector<array<int, 2>> coordinatesToFlip;
    vector<array<int, 2>> resultArray;
    array<int, 2> vectorTravel;
    array<State, 2> stoppingCondition = {_currentPlayer, UNUSED};
    int k = -1, l = -1;
    for(int line = 0; line < _nbLines; line++) for(int col = 0; col < _nbCols; col++) {
        if(_grid[line][col] == _currentPlayer) {
            k = -1;
            for(int i = line - 1 <  0 ? 0 : line - 1; i <= (line + 1 >= _nbLines ? _nbLines - 1 : line + 1); i++) {
                l = -1;
                for (int j = col - 1 < 0 ? 0 : col - 1; j <= (col + 1 >= _nbCols ? _nbCols - 1 : col + 1); j++) {
                    if((i != line || j != col) && find(begin(stoppingCondition), end(stoppingCondition), _grid[i][j]) == end(stoppingCondition)) {
                        vectorTravel = {k, l};
                        int multiplier = 1;
                        bool otherPlayerCase = false;
                        while(line + vectorTravel[0] * multiplier >= 0
                                && col + vectorTravel[1] * multiplier >= 0
                                && _grid[line + vectorTravel[0] * multiplier][col + vectorTravel[1] * multiplier] != UNUSED) {
                            if(_grid[line + vectorTravel[0] * multiplier][col + vectorTravel[1] * multiplier] == _currentPlayer) break;
                            if(_grid[line + vectorTravel[0] * multiplier][col + vectorTravel[1] * multiplier] != _currentPlayer) otherPlayerCase = true;
                            multiplier++;
                        }
                        if(otherPlayerCase) {
                            resultArray.push_back({line + vectorTravel[0] * multiplier, col + vectorTravel[1] * multiplier});
                        }
                    }
                    l++;
                }
                k++;
            }
        }
    }
    return resultArray;
}

State Othello::WhoWon() const {
    int playerOneCounter = 0, playerTwoCounter = 0;
    for (int i = 0; i < _nbLines; i++) for (int j = 0; j < _nbCols; j++) {
        if(_grid[i][j] == PLAYER_ONE) playerOneCounter++;
        else if (_grid[i][j] == PLAYER_TWO) playerTwoCounter++;
    }
    if(playerOneCounter == playerTwoCounter) return NONEXISTENT;
    return playerOneCounter > playerTwoCounter ? PLAYER_ONE : PLAYER_TWO;
}

EnumGame Othello::className() const {
    return OTHELLO;
}

bool Othello::CanPlay() const {
    return WhereCanPlay().size() != 0;
}

void Othello::Play(string playerOneName, string playerTwoName) {
    int line = -1, column = -1;
    bool retry;

    cout << "Places with a '3' are palces where current player can play." << endl;
    cout << DisplayGrid() << endl;
    while(CanPlay()) {
        cout << (_currentPlayer == PLAYER_ONE ? playerOneName : playerTwoName) << "'s turn." << endl;
        retry = false;
        while(line < 1 || line > _nbLines) {
            cout << "Choose line to place your Cell" << endl;
            cin >> line;
        }
        while(column < 1 || column > _nbCols) {
            cout << "Choose column to place your Cell" << endl;
            cin >> column;
        }
        try {
            ChooseCell(line - 1, column - 1);
        }  catch (string error) {
            cout << error << endl;
            retry = true;
        }

        if(!retry) {
            if(_currentPlayer == PLAYER_ONE) _currentPlayer = PLAYER_TWO;
            else _currentPlayer = PLAYER_ONE;
        }
        line = -1;
        column = -1;
        cout << DisplayGrid() << endl;
    }
    if(WhoWon() == PLAYER_ONE) cout << playerOneName << " Won!" << endl;
    else if(WhoWon() == PLAYER_TWO) cout << playerTwoName << " Won!" << endl;
    else cout << "Equality." << endl;
}
