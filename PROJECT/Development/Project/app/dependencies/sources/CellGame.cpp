#include "../headers/CellGame.h"
#include "../headers/Morpion.h"
#include "../headers/PowerFour.h"
#include "../headers/Othello.h"

using namespace std;

CellGame::CellGame(int lines, int cols) : _nbLines(lines), _nbCols(cols), _currentPlayer(PLAYER_ONE) {
    _grid.resize(_nbLines, vector<State>(_nbCols, UNUSED));
}

unique_ptr<CellGame> CellGame::CreateCellGame(EnumGame choice) {
    unique_ptr<CellGame> result;
    switch (choice) {
        case MORPION:
            result.reset(new Morpion());
            break;
        case POWERFOUR:
            result.reset(new PowerFour());
            break;
        case OTHELLO:
            result.reset(new Othello());
            break;
        default:
            throw "Game does not exist.";
    }
    return result;
}

bool CellGame::CheckState(int currentState, State stateToVerify) {
    return  currentState == stateToVerify;
}

void CellGame::ChooseCell(int cellLine, int cellColumn) {
    _grid[cellLine][cellColumn] = _currentPlayer;
}

void CellGame::Read(const QJsonObject &json) {
    if(json.contains("_grid") && json["_grid"].isArray()) {
        QJsonArray gridArray = json["_grid"].toArray();
        _grid.resize(_nbLines, vector<State>(_nbCols));
        for(int i = 0; i < _nbLines; i++) {
            for(int j = 0; j < _nbCols; j++) {
                _grid[i][j] = (State)gridArray[i * _nbLines + j].toInt();
            }
        }
    }
    if(json.contains("_currentPlayer") && json["_currentPlayer"].isDouble()) _currentPlayer = (State)json["_currentPlayer"].toDouble();
}
void CellGame::Write(QJsonObject &json) const {
    QJsonArray gridArray;
    for(int i = 0; i < _nbLines; i++) {
        for(int j = 0; j < _nbCols; j++) {
            gridArray.push_back(_grid[i][j]);
        }
    }
    json["_grid"] = gridArray;
    json["_currentPlayer"] = _currentPlayer;
    json["gameType"] = className();
}

string CellGame::DisplayGrid() const {
    string result = "";
    for (int i = 0; i < _nbLines ; i++) {
        for (int j = 0; j < _nbCols ; j++) result += to_string(_grid[i][j]);
        result += "\n";
    }
    return result;
}
