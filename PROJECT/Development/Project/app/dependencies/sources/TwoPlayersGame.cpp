#include "../headers/TwoPlayersGame.h"

using namespace std;

TwoPlayersGame::TwoPlayersGame() : _playerOne(), _playerTwo() {}

TwoPlayersGame::TwoPlayersGame(string pseudoPlayerOne, string pseudoPlayerTwo) : _playerOne(pseudoPlayerOne), _playerTwo(pseudoPlayerTwo) {}

Player TwoPlayersGame::GetPlayerOne() const {
    return _playerOne;
}

string TwoPlayersGame::GetPseudoPlayerOne() const {
    return _playerOne.GetPseudo();
}

void TwoPlayersGame::SetPseudoPlayerOne(string pseudo) {
    _playerOne.SetPseudo(pseudo);
}

Player TwoPlayersGame::GetPlayerTwo() const {
    return _playerTwo;
}

string TwoPlayersGame::GetPseudoPlayerTwo() const {
    return _playerTwo.GetPseudo();
}

void TwoPlayersGame::SetPseudoPlayerTwo(string pseudo) {
    _playerTwo.SetPseudo(pseudo);
}

void TwoPlayersGame::Read(const QJsonObject &json) {
    if(json.contains("_playerOne") && json["_playerOne"].isString()) _playerOne.SetPseudo(json["_playerOne"].toString().toStdString());
    if(json.contains("_playerTwo") && json["_playerTwo"].isString()) _playerTwo.SetPseudo(json["_playerTwo"].toString().toStdString());
}

void TwoPlayersGame::Write(QJsonObject &json) const {
    json["_playerOne"] = QString::fromStdString(_playerOne.GetPseudo());
    json["_playerTwo"] = QString::fromStdString(_playerTwo.GetPseudo());
}
