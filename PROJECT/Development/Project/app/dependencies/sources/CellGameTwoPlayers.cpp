#include "../headers/CellGameTwoPlayers.h"

using namespace std;

CellGameTwoPlayers::CellGameTwoPlayers() : _players(), _cellGame(nullptr) {
    LoadOrNewGame();
}

void CellGameTwoPlayers::AskPlayersName() {
    string pseudoPlayerOne, pseudoPlayerTwo;

    cout << "Choose Player one's Name : " << endl;
    cin >> pseudoPlayerOne;
    cout << "Choose Player two's Name : " << endl;
    cin >> pseudoPlayerTwo;
    _players.SetPseudoPlayerOne(pseudoPlayerOne);
    _players.SetPseudoPlayerTwo(pseudoPlayerTwo);
}

void CellGameTwoPlayers::ChooseGameType()
{
    int gameChoice = 0;

    while(gameChoice < 1 || gameChoice > 3)
    {
        cout << "Choose your game : " << endl;
        cout << "1. Morpion " << endl;
        cout << "2. PowerFour " << endl;
        cout << "3. Othello " << endl;

        cin >> gameChoice;
    }
    switch (gameChoice) {
        case 1:
            _cellGame = CellGame::CreateCellGame(MORPION);
            break;
        case 2:
            _cellGame = CellGame::CreateCellGame(POWERFOUR);
            break;
        case 3:
            _cellGame = CellGame::CreateCellGame(OTHELLO);
            break;
    }
}

void CellGameTwoPlayers::Write(QJsonObject &json) const {
    _players.Write(json);
    _cellGame->Write(json);
}

void CellGameTwoPlayers::Read(const QJsonObject &json) {
    if(json.contains("gameType") && json["gameType"].isDouble()) {
        EnumGame gameType = (EnumGame)json["gameType"].toDouble();
        _cellGame = CellGame::CreateCellGame(gameType);
    }
    _players.Read(json);
    _cellGame->Read(json);
}

void CellGameTwoPlayers::LoadOrNewGame() {
    if(QFile::exists(QString::fromStdString(SAVE_FILE_NAME))) {
        int choice = 0;
        while(choice < 1 || choice > 3)
        {
            cout << "Do you want to: " << endl;
            cout << "1. Prepare a new Game (Will remove current save)" << endl;
            cout << "2. Load saved Game" << endl;

            cin >> choice;
        }
        switch (choice) {
            case 1:
                PrepareNewGame();
                break;
            case 2:
                LoadGame();
                break;
        }
    } else {
        PrepareNewGame();
    }
    Play();
}

void CellGameTwoPlayers::PrepareNewGame() {
    AskPlayersName();
    ChooseGameType();
}

void CellGameTwoPlayers::SaveGame() {
    QFile saveFile(QString::fromStdString(SAVE_FILE_NAME));

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
    }

    QJsonObject cellGameObject;
    Write(cellGameObject);
    saveFile.write(QJsonDocument(cellGameObject).toJson());
}

void CellGameTwoPlayers::LoadGame() {
    QFile loadFile(QString::fromStdString(SAVE_FILE_NAME));

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
    }
    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    Read(loadDoc.object());
}

void CellGameTwoPlayers::ThreadSave(bool &gameFinished) {
    while(!gameFinished) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        SaveGame();
    }
    // Game is Finished, we do not want a save
    QFile::remove(QString::fromStdString(SAVE_FILE_NAME));
}

void CellGameTwoPlayers::Play()
{
    bool gameFinished = false;
    thread threadSave([this, &gameFinished] { ThreadSave(gameFinished); });
    _cellGame->Play(_players.GetPseudoPlayerOne(), _players.GetPseudoPlayerTwo());
    gameFinished = true;
    threadSave.join();
}
