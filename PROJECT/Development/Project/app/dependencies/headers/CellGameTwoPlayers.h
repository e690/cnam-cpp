#ifndef CELLGAMETWOPLAYERS_H
#define CELLGAMETWOPLAYERS_H

#include <memory>
#include <thread>
#include <chrono>
#include <iostream>
#include <QFile>
#include <QJsonDocument>

#include "TwoPlayersGame.h"
#include "CellGame.h"
#include "../interfaces/ISerializable.h"
#include "../enums/EnumGame.h"

const std::string SAVE_FILE_NAME = "save.json";

class CellGameTwoPlayers : ISerializable {
public:

    CellGameTwoPlayers();

private:
    TwoPlayersGame _players;
    std::unique_ptr<CellGame> _cellGame;

    virtual void Read(const QJsonObject &json);
    virtual void Write(QJsonObject &json) const;
    void ThreadSave(bool &gameFinished);
    void SaveGame();
    void LoadOrNewGame();
    void Play();
    void AskPlayersName();
    void ChooseGameType();
    void LoadGame();
    void PrepareNewGame();


};

#endif // CELLGAMETWOPLAYERS_H
