#ifndef POWERFOUR_H
#define POWERFOUR_H

#include "CellGameLineCondition.h"

class PowerFour final : public CellGameLineCondition {
public:
    PowerFour();

    void Play(std::string playerOneName, std::string playerTwoName);
private:
    bool IsLineFull(State playerIndex, int lineIndex) const;
    bool IsColumnFull(State playerIndex, int columnIndex) const;
    bool IsDiagonalFull(State playerIndex) const;
    bool IsWinning(std::vector<State> vector, State stateToVerify) const;
private:
    virtual EnumGame className() const;
};

#endif // POWERFOUR_H
