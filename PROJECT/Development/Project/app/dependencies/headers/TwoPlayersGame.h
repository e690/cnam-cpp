#ifndef TWOPLAYERSGAME_H
#define TWOPLAYERSGAME_H

#include "Player.h"
#include "../interfaces/ISerializable.h"

class TwoPlayersGame : public ISerializable {
public:

    TwoPlayersGame();
    TwoPlayersGame(std::string pseudoPlayerOne, std::string pseudoPlayerTwo);
    Player GetPlayerOne() const;
    std::string GetPseudoPlayerOne() const;
    void SetPseudoPlayerOne(std::string pseudo);
    Player GetPlayerTwo() const;
    std::string GetPseudoPlayerTwo() const;
    void SetPseudoPlayerTwo(std::string pseudo);
    virtual void Read(const QJsonObject &json);
    virtual void Write(QJsonObject &json) const;

private:
    Player _playerOne;
    Player _playerTwo;
};

#endif // TWOPLAYERSGAME_H
