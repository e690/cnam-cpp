#ifndef PLAYER_H
#define PLAYER_H

#include <string>

class Player {
public:
    Player();
    Player(std::string pseudo);
    std::string GetPseudo() const;
    void SetPseudo(std::string pseudo);
private:
    std::string _pseudo;
};

#endif // PLAYER_H
