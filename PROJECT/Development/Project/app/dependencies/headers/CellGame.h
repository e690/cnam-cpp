#ifndef CELLGAME_H
#define CELLGAME_H

#include <string>
#include <memory>
#include <vector>

#include "../interfaces/ISerializable.h"

#include "../enums/State.h"
#include "../enums/EnumGame.h"

class CellGame : public ISerializable {
public:
    CellGame();
    CellGame(int lines, int cols);
    // Factory Method
    static std::unique_ptr<CellGame> CreateCellGame(EnumGame choice);
    virtual void ChooseCell(int cellLine, int cellColumn);
    virtual std::string DisplayGrid() const;
    virtual void Read(const QJsonObject &json);
    virtual void Write(QJsonObject &json) const;
    virtual void Play(std::string playerOneName, std::string playerTwoName) = 0;

protected:
    int _nbLines;
    int _nbCols;
    State _currentPlayer;
    std::vector<std::vector<State>> _grid;
    static bool CheckState(int currentState, State stateToVerify);

private:
    virtual EnumGame className() const = 0;
};

#endif // CELLGAME_H
