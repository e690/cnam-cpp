#ifndef STATE_H
#define STATE_H

enum State {NONEXISTENT = -1, UNUSED = 0, PLAYER_ONE = 1, PLAYER_TWO = 2, PLAYABLE = 3};

#endif // STATE_H
