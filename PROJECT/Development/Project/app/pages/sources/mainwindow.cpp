#include "../headers/mainwindow.h"
#include "ui_Mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnQuitMainMenu_clicked()
{
    this->close();
}

void MainWindow::on_btn_NewGame_clicked()
{
    MenuChooseGame *m_chooseGame = new MenuChooseGame;
    m_chooseGame->show();
    this->close();
}

