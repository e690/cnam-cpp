#include "../headers/menuchoosegame.h"
#include "../headers/mainwindow.h"
#include "ui_menuchoosegame.h"

MenuChooseGame::MenuChooseGame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuChooseGame)
{
    ui->setupUi(this);
}

MenuChooseGame::~MenuChooseGame()
{
    delete ui;
}

void MenuChooseGame::on_btnBackMainMenu_clicked()
{
    MainWindow *mainWindow = new MainWindow;
    mainWindow->show();
    this->close();

}



