#ifndef MENUCHOOSEGAME_H
#define MENUCHOOSEGAME_H

#include <QWidget>

namespace Ui {
class MenuChooseGame;
}

class MenuChooseGame : public QWidget
{
    Q_OBJECT

public:
    explicit MenuChooseGame(QWidget *parent = nullptr);
    ~MenuChooseGame();

private slots:
    void on_btnBackMainMenu_clicked();

private:
    Ui::MenuChooseGame *ui;
};

#endif // MENUCHOOSEGAME_H
