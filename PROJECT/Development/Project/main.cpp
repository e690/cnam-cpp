#include "app/pages/headers/mainwindow.h"
#include "app/dependencies/headers/CellGameTwoPlayers.h"

#include <iostream>
#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QFile>
#include <QJsonDocument>

int main(int argc, char *argv[])
{
    QApplication application(argc, argv);
    CellGameTwoPlayers cellGame = CellGameTwoPlayers();


    /*// Translation import
    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "Project_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            application.installTranslator(&translator);
            break;
        }
    }
    MainWindow w;
    w.show();*/
    return application.exec();
}
