#include <iostream>

#include "dependencies/CellGameTwoPlayers.h"

using namespace std;

int main()
{
    CellGameTwoPlayers cellGame = CellGameTwoPlayers{"Yoann", "Rayane"};

    cellGame.Play();

    return 0;
}
