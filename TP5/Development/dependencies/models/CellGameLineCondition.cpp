# include "../CellGameLineCondition.h"

using namespace std;

CellGameLineCondition::CellGameLineCondition(int lines, int cols) : CellGame(lines, cols) {}

bool CellGameLineCondition::PlayerWon(State playerIndex) const {
    for(int i = 0; i < _nbLines; i++) if(IsLineFull(playerIndex, i)) return true;
    for(int i = 0; i < _nbCols; i++) if(IsColumnFull(playerIndex, i)) return true;
    if(IsDiagonalFull(playerIndex)) return true;
    return false;
}

void CellGameLineCondition::ChooseCell(int cellLine, int cellColumn) {
    if(CheckState(_grid[cellLine][cellColumn], UNUSED)) {
        CellGame::ChooseCell(cellLine, cellColumn);
    } else {
        throw string("Cell already choosen.");
    }
}
