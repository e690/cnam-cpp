#include "../Player.h"

Player::Player() {

}

Player::Player(std::string pseudo) {
    this->_pseudo = pseudo;
}

std::string Player::GetPseudo() const {
    return this->_pseudo;
}

void Player::SetPseudo(std::string pseudo) {
    this->_pseudo = pseudo;
}
