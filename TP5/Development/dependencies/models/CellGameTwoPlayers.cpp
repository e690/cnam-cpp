#include "../CellGameTwoPlayers.h"

using namespace std;

CellGameTwoPlayers::CellGameTwoPlayers(string pseudoPlayerOne, string pseudoPlayerTwo) : _players ( pseudoPlayerOne, pseudoPlayerTwo )
{
    ChooseGameType();
}

void CellGameTwoPlayers::ChooseGameType()
{
    int gameChoice = 0;

    while(gameChoice <= 0 || gameChoice > 3)
    {
        cout << "Choose your game : " << endl;
        cout << "1. Morpion " << endl;
        cout << "2. PowerFour " << endl;
        cout << "3. Othello " << endl;

        cin >> gameChoice;
    }
    switch (gameChoice) {
        case 1:
            _cellGame = unique_ptr<CellGame> (new Morpion());
            break;
        case 2:
            _cellGame = unique_ptr<CellGame> (new PowerFour());
            break;
        case 3:
            _cellGame = unique_ptr<CellGame> (new Othello());
            break;
    }
}

void CellGameTwoPlayers::Play()
{
    Player playerOne = _players.GetPlayerOne();
    Player playerTwo = _players.GetPlayerTwo();
    _cellGame->Play(playerOne.GetPseudo(), playerTwo.GetPseudo());
}
