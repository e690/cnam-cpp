#include "../CellGame.h"

using namespace std;

CellGame::CellGame(int lines, int cols) {
    _nbLines = lines;
    _nbCols = cols;
    _currentPlayer = PLAYER_ONE;
    _grid.resize(_nbLines, vector<State>(_nbCols, UNUSED));
}

bool CellGame::CheckState(int currentState, State stateToVerify) {
    return  currentState == stateToVerify;
}

void CellGame::ChooseCell(int cellLine, int cellColumn) {
    _grid[cellLine][cellColumn] = _currentPlayer;
}

string CellGame::DisplayGrid() const {
    string result = "";
    for (int i = 0; i < _nbLines ; i++) {
        for (int j = 0; j < _nbCols ; j++) result += to_string(_grid[i][j]);
        result += "\n";
    }
    return result;
}
