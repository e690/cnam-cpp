#ifndef CELLGAME_H
#define CELLGAME_H

#include "./enums/State.h"
#include <string>
#include <vector>

class CellGame
{
public:
    CellGame(int lines, int cols);
    virtual void ChooseCell(int cellLine, int cellColumn);
    virtual std::string DisplayGrid() const;

    virtual void Play(std::string playerOneName, std::string playerTwoName) = 0;

protected:
    int _nbLines;
    int _nbCols;
    State _currentPlayer;
    std::vector<std::vector<State>> _grid;
    static bool CheckState(int currentState, State stateToVerify);

};

#endif // CELLGAME_H
