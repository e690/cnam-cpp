#ifndef CELLGAMELINECONDITION_H
#define CELLGAMELINECONDITION_H

#include "CellGame.h"

class CellGameLineCondition : public CellGame
{
public:
    CellGameLineCondition(int lines, int cols);

    virtual void ChooseCell(int cellLine, int cellColumn);
    bool PlayerWon(State playerIndex) const;
    virtual void Play(std::string playerOneName, std::string playerTwoName) = 0;
protected:
    virtual bool IsLineFull(State playerIndex, int lineIndex) const = 0;
    virtual bool IsColumnFull(State playerIndex, int columnIndex) const = 0;
    virtual bool IsDiagonalFull(State playerIndex) const = 0;
    virtual bool IsWinning(std::vector<State> vector, State stateToVerify) const = 0;
};

#endif // CELLGAMELINECONDITION_H
