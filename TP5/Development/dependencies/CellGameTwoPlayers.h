#ifndef CELLGAMETWOPLAYERS_H
#define CELLGAMETWOPLAYERS_H

#include <memory>
#include <iostream>

#include "TwoPlayersGame.h"
#include "CellGame.h"
#include "Morpion.h"
#include "PowerFour.h"
#include "Othello.h"

class CellGameTwoPlayers
{
public:

    CellGameTwoPlayers(std::string pseudoPlayerOne, std::string pseudoPlayerTwo);
    void ChooseGameType();
    void Play();

private:

    TwoPlayersGame _players;
    std::unique_ptr<CellGame> _cellGame;

};

#endif // CELLGAMETWOPLAYERS_H
