#ifndef OTHELLO_H
#define OTHELLO_H

#include <array>

#include "CellGame.h"

class Othello final : public CellGame
{
public:
    Othello();

    virtual void ChooseCell(int cellLine, int cellColumn);

    State WhoWon() const;
    std::string DisplayGrid() const;
    void Play(std::string playerOneName, std::string playerTwoName);
protected:
    std::vector<std::array<int, 2>> WhereCanPlay() const;
    bool CanPlay() const;
    void FlipCells(int cellLine, int cellColumn);
};

#endif // OTHELLO_H
