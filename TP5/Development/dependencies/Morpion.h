#ifndef MORPION_H
#define MORPION_H

#include "CellGameLineCondition.h"
#include "Player.h"


class Morpion final : public CellGameLineCondition
{
public:
    Morpion();
    void Play(std::string playerOneName, std::string playerTwoName);
private:
    bool IsLineFull(State playerIndex, int lineIndex) const;
    bool IsColumnFull(State playerIndex, int columnIndex) const;
    bool IsDiagonalFull(State playerIndex) const;
    bool IsWinning(std::vector<State> vector, State stateToVerify) const;
};

#endif // MORPION_H
