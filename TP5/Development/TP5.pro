TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        dependencies/models/CellGame.cpp \
        dependencies/models/CellGameLineCondition.cpp \
        dependencies/models/CellGameTwoPlayers.cpp \
        dependencies/models/Morpion.cpp \
        dependencies/models/Othello.cpp \
        dependencies/models/Player.cpp \
        dependencies/models/PowerFour.cpp \
        dependencies/models/TwoPlayersGame.cpp \
        main.cpp

HEADERS += \
    dependencies/CellGame.h \
    dependencies/CellGameLineCondition.h \
    dependencies/CellGameTwoPlayers.h \
    dependencies/Morpion.h \
    dependencies/Othello.h \
    dependencies/Player.h \
    dependencies/PowerFour.h \
    dependencies/TwoPlayersGame.h \
    dependencies/enums/State.h \
    dependencies/interfaces/CellGameLineCondition.h
