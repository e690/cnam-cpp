#include <iostream>
#include <cstdlib>

#include "header.h"

using namespace std;

int main() {
    int playerOneRally, playerTwoRally, status;

    cout << "Input 1 for a Match, 2 for a Result";
    cin >> status;

    switch (status)
    {
    case 1:
        {
            playerOneRally = 0;
            playerTwoRally = 0;
            int rallyWinner, absRally;
            bool match = true;
            while(match) {
                cout << "Did Player 1 or 2 win the rally ? ";
                cin >> rallyWinner;
                if(rallyWinner == 1 || rallyWinner == 2) {
                    rallyWinner == 1 ? playerOneRally++ : playerTwoRally++;
                    absRally = abs(playerOneRally - playerTwoRally);
                    if((playerOneRally >= 4 || playerTwoRally >= 4) && absRally >= 2) {
                        match = false;
                    }
                    cout << tennisGameResult(playerOneRally,playerTwoRally) << endl;
                } else {
                    cout << "Please Input a good number " << endl;
                }
            }
            break;
        }
    case 2:
        {
            cout << "How much rallies did First player win ? ";
            cin >> playerOneRally;

            cout << "How much rallies did Second player win ? ";
            cin >> playerTwoRally;

            cout << tennisGameResult(playerOneRally,playerTwoRally) << endl;
            break;
        }
    default:
        {
            cout << "Please Input a good number" << endl;
            break;
        }
    }
    return 0;
}