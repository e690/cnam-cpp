#include <iostream>
#include <cstdlib>

using namespace std;


/**
 * Determine what is the result of a Tennis game with won rallies of each player.
 */
string tennisGameResult(int playerOneRally, int playerTwoRally) {
    int playerOneScore = 0, playerTwoScore = 0;
    bool endGameOne = false, endGameTwo = false;

    if(playerOneRally >= 4 || playerTwoRally >= 4) {
        if(playerOneRally - playerTwoRally >= 2) {
            return "First Player won.";
        }
        else if(playerOneRally - playerTwoRally <= -2)  {
            return "First Player won.";
        } else {
            switch (playerOneRally - playerTwoRally)
            {
            case 1:
                return "Player One Advantage";
                break;
            
            case 0:
                return "40 Love";
                break;
            case -1:
                return "Player Two Advantage";
                break;
            default:
                break;
            }
        }
    } else {
        playerOneScore = playerOneRally == 0 ? 0 : playerOneRally % 3 == 0 ? 40 : playerOneRally % 2 == 0 ? 30 : 15;
        playerTwoScore = playerTwoScore == 0 ? 0 : playerTwoScore % 3 == 0 ? 40 : playerTwoScore % 2 == 0 ? 30 : 15;
        return "P1 - " + to_string(playerOneScore) + " P2 - "+ to_string(playerTwoScore);
    }
}   