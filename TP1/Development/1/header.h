/**
 * Sum two int Numbers
 */
int sumTwoInts(int firstValue, int secondValue) {
    return (firstValue + secondValue);
}

/**
 * Invert values of two Ints
 */
void invertValuesInts(int* firstValue, int* secondValue) {
    int temp = *firstValue;
    *firstValue = *secondValue;
    *secondValue = temp;
}

/**
 * Third Integer becomes the result of two other's Sum
 * Pointers Form
 */
void pointerSumInts(int* firstValue, int* secondValue, int* thirdValue) {
    *thirdValue = *firstValue + *secondValue;
}

/**
 * Third Integer becomes the result of two other's Sum
 * Reference Form
 */
void referenceSumInts(int& firstValue, int& secondValue, int& thirdValue) {
    int &reference = thirdValue;
    reference = firstValue + secondValue;
}

/**
 * Sort an array
 */
void ascendingSortArray(int array[], int arraySize) {
    int sortIndex = 1;

    while (sortIndex < arraySize){
        if (array[sortIndex] < array[sortIndex-1]){
            invertValuesInts(&array[sortIndex], &array[sortIndex-1]);
            sortIndex = 1;
        } else {
            sortIndex++;
        }
    }
}