#include <iostream>
#include <cstdlib>

#include "header.h"

using namespace std;


int main() {
	int array[10];
	int arraySize = sizeof(array) / sizeof(int);

	// Random Numbers in array, and display in stdout
	cout << "random array : [";
	for (int i = 0; i < arraySize; i++)
	{
		array[i] = rand();
		cout << array[i];
		if(i != arraySize -1) {
			cout << ", ";
		}
	}
	cout << "]" << endl;

	ascendingSortArray(array, arraySize);

	cout << "sorted array : [";
	for (int i = 0; i < sizeof(array) / sizeof(int); i++)
	{
		cout << array[i];
		if(i != sizeof(array) / sizeof(int) -1) {
			cout << ", ";
		}
	}
	cout << "]" << endl;
	return 0;
}