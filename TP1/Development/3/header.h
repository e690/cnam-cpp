#include <iostream>
#include <cstdlib>

using namespace std;

// Part One

/**
 * Ask First and Last Name then Salute
 */
void HelloWorld() {
    string firstName, lastName;

    cout << "What is your Full Name ? (Firstname Lastname) " << endl;
    cin >> firstName >> lastName;
    for (auto &&lastNameChar : lastName)
    {
        lastNameChar = toupper(lastNameChar);
    }
    
    firstName[0] = toupper(firstName[0]);
    for (int i = 1; i < firstName.size(); i++)
    {
        firstName[i] = tolower(firstName[i]);
    }
    
    cout << "Hello " << firstName << " " << lastName << " !" << endl;
}

/**
 * Try to find the right random generated number
 */
void findNumber() {
    int generatedNumber = rand() % 1000, guess = -1, nbTries = 0;
    string userInfo = "";

    while (guess != generatedNumber)
    {
        cout << "Guess a number between 0 and 1000 : " << endl;
        cin >> guess;
        nbTries++;

        if(guess < generatedNumber) {
            userInfo = "Your guess is lighter.";
        } else if(guess > generatedNumber) {
            userInfo = "Your guess is stronger.";
        } else {
            userInfo = "Congrats ! You found the right number " + to_string(guess) + " in " + to_string(nbTries) + " tries.";
        }

        cout << userInfo << endl;
    }
}

/**
 * Finding a number with dichotomic method
 */
void DichotomySearch() {
    int minimum = 0, maximum = 1000, nbTries = 0, guess = -1, numberToFind;
    
    cout << "Please Input the number which computer will try to find between 0 and 1000" << endl;
    cin >> numberToFind;

    while (guess != numberToFind) {
        guess = (minimum + maximum) / 2;
        nbTries++;
        if(guess % 2 != 0) {
            guess++;
        }
        if(guess < numberToFind) {
            minimum = guess;
        } else if (guess > numberToFind) {
            maximum = guess;
        } else {
            cout << "Computer found the right number in " << nbTries << " tries." << endl;
        }
    }
}
