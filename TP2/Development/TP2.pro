TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        dependencies/models/Cercle.cpp \
        dependencies/models/Triangle.cpp \
        dependencies/models/Point.cpp \
        dependencies/models/Rectangle.cpp \
        main.cpp

HEADERS += \
    dependencies/Cercle.h \
    dependencies/Point.h \
    dependencies/Rectangle.h \
    dependencies/Triangle.h
