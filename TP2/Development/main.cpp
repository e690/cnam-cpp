#include <iostream>
#include <sstream>
#include <iterator>
#include "dependencies/Point.h"
#include "dependencies/Rectangle.h"
#include "dependencies/Cercle.h"
#include "dependencies/Triangle.h"

using namespace std;

int main(int argc, char const *argv[])
{
    Point point{2, 3}, onCercle{-2, -3}, underCercle{-1, -1};
    Rectangle rectangle{2, 10, Point{1, 2}};
    Triangle triangle{Point{0,0}, Point{0,1}, Point{1,0}};
    Cercle cercle{Point{2 , 0}, 10};
    ostringstream oss;
    vector<double> lengths = triangle.Lengths();

    if(!lengths.empty()) {
        // Convert vector array to string, while spliting with a comma
        // Not considering last element to avoid trailing comma
        copy(lengths.begin(), lengths.end()-1,
        ostream_iterator<double>(oss, ", "));

        // Now add the last element with no delimiter
        oss << lengths.back();
    }

    cout << point.Display() << endl;

    cout << rectangle.Display() << endl;
    cout << "Perimeter: " << rectangle.Perimeter() <<endl;
    cout << "Surface: " << rectangle.Surface() << endl;

    cout << cercle.Display() << endl;
    cout << "Perimeter: " << cercle.Perimeter() <<endl;
    cout << "Surface: " << cercle.Surface() << endl;
    cout << "Point onCercle is on circle : " << (cercle.OnCercle(onCercle) ? "true" : "false") << endl;
    cout << "Point underCercle is inside circle : " << (cercle.InCercle(underCercle) ? "true" : "false") << endl;

    cout << triangle.Display() << endl;
    cout << "Lengths: [" << oss.str() << "]" << endl;
    cout << "Base Length: " << triangle.Base() << endl;
    cout << "Area: " << triangle.Area() << endl;
    cout << "Height: " << triangle.Height() << endl;
    cout << "Isoceles: " << (triangle.IsIsosceles() ? "true" : "false") << endl;
    cout << "Equilateral: " << (triangle.IsEquilateral() ? "true" : "false") << endl;
    cout << "Rectangle: " << (triangle.IsRightAngle() ? "true" : "false") << endl;

    return 0;
}
