#ifndef POINT_H
#define POINT_H

#include <string>

struct Point {
    double m_X;
    double m_Y;

    Point();
    Point(double _x, double _y);
    std::string Display();
};

#endif // POINT_H
