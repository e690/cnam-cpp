#ifndef CERCLE_H
#define CERCLE_H

#include <string>
#include "Point.h"

class Cercle
{
private:
    Point m_CENTER;
    int m_DIAMETER;
public:
    Cercle(Point _center, int _diameter);

    Point GetM_CENTER() const;
    void SetM_CENTER(Point m_CENTER);
    int GetM_DIAMETER() const;
    void SetM_DIAMETER(int m_DIAMETER);

    double Perimeter() const;
    double Surface() const;
    bool OnCercle(Point point) const;
    bool InCercle(Point point) const;
    std::string Display();
};

#endif // CERCLE_H
