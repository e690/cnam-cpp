#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <string>
#include "Point.h"

class Rectangle
{
private:
    int m_LENGTH;
    int m_WIDTH;
    Point m_STARTINGPOINT;

public:
    Rectangle(int _length, int _width, Point _startingPoint);

    int GetM_LENGTH() const;
    void SetM_LENGTH(int m_LENGTH);
    int GetM_WIDTH() const;
    void SetM_WIDTH(int m_WIDTH);
    Point GetM_STARTINGPOINT() const;
    void SetM_STARTINGPOINT(Point m_STARTINGPOINT);

    double Perimeter() const;
    double Surface() const;
    std::string Display();
};

#endif // RECTANGLE_H
