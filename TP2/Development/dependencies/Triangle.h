#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <string>
#include <vector>
#include "Point.h"

class Triangle
{
private:
    Point m_FIRSTPOINT;
    Point m_SECONDPOINT;
    Point m_THIRDPOINT;

    double Perimeter() const;
public:
    Triangle(Point _firstPoint, Point _secondPoint, Point _thirdPoint);

    Point GetM_FIRSTPOINT() const;
    void SetM_FIRSTPOINT(Point m_FIRSTPOINT);
    Point GetM_SECONDPOINT() const;
    void SetM_SECONDPOINT(Point m_SECONDPOINT);
    Point GetM_THIRDPOINT() const;
    void SetM_THIRDPOINT(Point m_THIRDPOINT);

    std::vector<double> Lengths() const;
    double Base() const;
    double Area() const;
    double Height() const;
    bool IsIsosceles() const;
    bool IsRightAngle() const;
    bool IsEquilateral() const;
    std::string Display();
};

#endif // TRIANGLE_H
