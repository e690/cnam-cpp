#include <string>
#include <algorithm>
#include <numeric>
#include <math.h>
#include "../Triangle.h"

using namespace std;

Triangle::Triangle(Point _firstPoint, Point _secondPoint, Point _thirdPoint) {
    this->m_FIRSTPOINT = _firstPoint;
    this->m_SECONDPOINT = _secondPoint;
    this->m_THIRDPOINT = _thirdPoint;
}

vector<double> Triangle::Lengths() const {
    vector<double> result;
    double temp = 0;
    for(int i = 0; i < 3; i++) {
        switch (i) {
            case 0 :
                temp = sqrt(pow(this->m_SECONDPOINT.m_X - this->m_FIRSTPOINT.m_X, 2) + pow(this->m_SECONDPOINT.m_Y - this->m_FIRSTPOINT.m_Y, 2));
                break;
            case 1 :
                temp = sqrt(pow(this->m_THIRDPOINT.m_X - this->m_SECONDPOINT.m_X, 2) + pow(this->m_THIRDPOINT.m_Y - this->m_SECONDPOINT.m_Y, 2));
                break;
            case 2 :
                temp = sqrt(pow(this->m_FIRSTPOINT.m_X - this->m_THIRDPOINT.m_X, 2) + pow(this->m_FIRSTPOINT.m_Y - this->m_THIRDPOINT.m_Y, 2));
                break;
        }
        result.push_back(temp);
    }
    return result;
}

Point Triangle::GetM_FIRSTPOINT() const {
    return this->m_SECONDPOINT;
}

void Triangle::SetM_FIRSTPOINT(Point m_FIRSTPOINT) {
    this->m_FIRSTPOINT = m_FIRSTPOINT;
}

Point Triangle::GetM_SECONDPOINT() const {
    return this->m_SECONDPOINT;
}

void Triangle::SetM_SECONDPOINT(Point m_SECONDPOINT) {
    this->m_FIRSTPOINT = m_SECONDPOINT;
}

Point Triangle::GetM_THIRDPOINT() const {
    return this->m_SECONDPOINT;
}

void Triangle::SetM_THIRDPOINT(Point m_THIRDPOINT) {
    this->m_FIRSTPOINT = m_THIRDPOINT;
}

double Triangle::Base() const {
    vector<double> temp = this->Lengths();
    return *max_element(begin(temp), end(temp));
}

double Triangle::Perimeter() const {
    vector<double> temp = this->Lengths();
    double sum = 0;
    return accumulate(temp.begin(), temp.end(), sum);
}

double Triangle::Area() const {
    vector<double> temp = this->Lengths();
    double demiPerimeter = this->Perimeter() / 2;
    double area = sqrt(demiPerimeter * (demiPerimeter - temp[0])* (demiPerimeter - temp[1])* (demiPerimeter - temp[2]));
    return area;
}

double Triangle::Height() const {
    return 2 * this->Area() /this->Base();
}

bool Triangle::IsIsosceles() const {
    vector<double> temp = this->Lengths();
    return temp[0] == temp[1] || temp[1] == temp[2] || temp[2] == temp[0];
}

bool Triangle::IsEquilateral() const {
    vector<double> temp = this->Lengths();
    return adjacent_find(temp.begin(), temp.end(), not_equal_to<>()) == temp.end();
}

bool Triangle::IsRightAngle() const {
    vector<double> temp = this->Lengths();
    double base = this->Base();
    temp.erase(remove(temp.begin(), temp.end(),base));
    return round(pow(base, 2)) == round(pow(temp[0], 2) + pow(temp[1], 2));
}

string Triangle::Display() {
    return "Triangle:\nm_FIRSTPOINT: (" + this->m_FIRSTPOINT.Display()
            + ")\nm_SECONDPOINT: (" + this->m_SECONDPOINT.Display()
            + ")\nm_THIRDPOINT: (" + this->m_THIRDPOINT.Display() + ")";
}
