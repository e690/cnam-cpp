#include <string>
#include <math.h>
#include "../Cercle.h"

using namespace std;

Cercle::Cercle(Point _center, int _diameter) {
    this->m_CENTER = _center;
    this->m_DIAMETER = _diameter;
}

Point Cercle::GetM_CENTER() const {
    return this->m_CENTER;
}

void Cercle::SetM_CENTER(Point m_CENTER) {
    this->m_CENTER = m_CENTER;
}

int Cercle::GetM_DIAMETER() const {
    return this->m_DIAMETER;
}

void Cercle::SetM_DIAMETER(int m_DIAMETER) {
    this->m_DIAMETER = m_DIAMETER;
}

double Cercle::Perimeter() const {
    return 2 * M_PI * (this->m_DIAMETER / 2);
}

double Cercle::Surface() const {
    return M_PI * pow(this->m_DIAMETER / 2, 2);
}

bool Cercle::OnCercle(Point point) const {
    double distance = sqrt(pow(this->m_CENTER.m_X - point.m_X, 2) + pow(this->m_CENTER.m_Y - point.m_Y, 2));
    return fabs(distance - (this->m_DIAMETER / 2)) < numeric_limits<double>::epsilon();
}

bool Cercle::InCercle(Point point) const {
    double distance = sqrt(pow(this->m_CENTER.m_X - point.m_X, 2) + pow(this->m_CENTER.m_Y - point.m_Y, 2));
    return distance < (this->m_DIAMETER / 2);
}

string Cercle::Display() {
    return "Cercle: \nm_CENTER: (" + this->m_CENTER.Display()
            + ")\nM_DIAMETER: " + to_string(this->m_DIAMETER);
}
