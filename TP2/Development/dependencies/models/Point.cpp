#include <string>
#include "../Point.h"

using namespace std;

Point::Point()
{
    this->m_X = 0;
    this->m_Y = 0;
}

Point::Point(double _x, double _y)
{
    this->m_X = _x;
    this->m_Y = _y;
}

string Point::Display()
{
    return "m_X = " + to_string(this->m_X) + " m_Y = " + to_string(this->m_Y);
}
