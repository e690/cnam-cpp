#include <string>
#include "../Rectangle.h"

using namespace std;

Rectangle::Rectangle(int _length, int _width, Point _startingPoint)
{
    this->m_LENGTH = _length;
    this->m_WIDTH = _width;
    this->m_STARTINGPOINT = _startingPoint;
}

int Rectangle::GetM_LENGTH() const {
        return this->m_LENGTH;
}

void Rectangle::SetM_LENGTH(int m_LENGTH) {
    this->m_LENGTH = m_LENGTH;
}

int Rectangle::GetM_WIDTH() const {
    return this->m_WIDTH;
}

void Rectangle::SetM_WIDTH(int m_WIDTH) {
    this->m_WIDTH = m_WIDTH;
}

Point Rectangle::GetM_STARTINGPOINT() const {
    return this->m_STARTINGPOINT;
}

void Rectangle::SetM_STARTINGPOINT(Point m_STARTINGPOINT) {
    this->m_STARTINGPOINT = m_STARTINGPOINT;
}

double Rectangle::Perimeter() const {
    return (this->m_LENGTH + this->m_WIDTH) * 2;
}

double Rectangle::Surface() const {
    return this->m_LENGTH * this->m_WIDTH;
}

string Rectangle::Display() {
    return "Rectangle: \nm_LENGTH: " + to_string(this->m_LENGTH)
            + "\nm_WIDTH: " + to_string(this->m_WIDTH)
            + "\nm_STARTINGPOINT: (" + this->m_STARTINGPOINT.Display() + ")";
}
